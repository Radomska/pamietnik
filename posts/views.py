from django.shortcuts import render
from .models import Topic
from .models import Entry
from django.contrib.auth.decorators import login_required


from django.http import HttpResponseRedirect, Http404
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .forms import TopicForm, EntryForm
# Create your views here.

def home(request):
    return render(request, 'home.html')

@login_required
def topics(request):
    topics = Topic.objects.filter(user=request.user).order_by('date_added')
    setting = {'topics': topics}
    return render(request, 'topics.html', setting)


@login_required
def topic(request, topic_id):
    topic = Topic.objects.get(id=topic_id)
    if topic.user != request.user:
        raise Http404
    entries = topic.entry_set.order_by('-date_added')
    setting = {'topic': topic, 'entries': entries}
    return render(request, 'topic.html', setting)


@login_required
def new_topic(request):
    if request.method != 'POST':
        form = TopicForm()
    else:
        form = TopicForm(request.POST)
        if form.is_valid():
            new_topic = form.save(commit=False)
            new_topic.user = request.user
            new_topic.save()
            return HttpResponseRedirect(reverse('posts:topics'))

    setting = {'form': form}
    return render(request, 'new_topic.html', setting)

@login_required
def new_entry(request, topic_id):
    topic = Topic.objects.get(id=topic_id)

    if request.method != 'POST':
        form = EntryForm()
    else:
        form = EntryForm(data=request.POST)
        if form.is_valid():
            new_entry = form.save(commit=False)
            new_entry.topic = topic
            new_entry.save()
            return HttpResponseRedirect(reverse('posts:topic', args=[topic_id]))

    setting = {'topic': topic, 'form': form}
    return render(request, 'new_entry.html', setting)


@login_required
def edit_entry(request, entry_id):
    entry = Entry.objects.get(id=entry_id)
    topic = entry.topic
    if topic.user != request.user:
        raise Http404

    if request.method != 'POST':
        form = EntryForm(instance=entry)
    else:
        form = EntryForm(instance=entry, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('posts:topic', args=[topic.id]))

    setting = {'entry' : entry, 'topic' : topic, 'form' : form}
    return render(request, 'edit_entry.html', setting)


@login_required
def delete_note(request, entry_id):
    note = Entry.objects.get(id=entry_id)
    topic = note.topic

    note.delete()

    return HttpResponseRedirect(reverse('posts:topic', args=[topic.id]))