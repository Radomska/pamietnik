from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView

from . import views


urlpatterns = [
    path('login/', LoginView.as_view (template_name="login_owner.html"), name="login"),
    path('logout/', LogoutView.as_view(), name="logout_page"),
    path('register_user/', views.register_user, name='register_user'),
    ]